<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.webdesign-studenten.nl
 * @since      1.0.0
 *
 * @package    Woocommerce_dashboard_pro
 * @subpackage Woocommerce_dashboard_pro/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woocommerce_dashboard_pro
 * @subpackage Woocommerce_dashboard_pro/includes
 * @author     Webdesign Studenten <info@webdesign-studenten.nl>
 */
class Woocommerce_dashboard_pro_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
