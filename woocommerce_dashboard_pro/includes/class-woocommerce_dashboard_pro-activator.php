<?php

/**
 * Fired during plugin activation
 *
 * @link       www.webdesign-studenten.nl
 * @since      1.0.0
 *
 * @package    Woocommerce_dashboard_pro
 * @subpackage Woocommerce_dashboard_pro/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woocommerce_dashboard_pro
 * @subpackage Woocommerce_dashboard_pro/includes
 * @author     Webdesign Studenten <info@webdesign-studenten.nl>
 */
class Woocommerce_dashboard_pro_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
