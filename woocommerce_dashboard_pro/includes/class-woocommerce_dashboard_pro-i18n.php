<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       www.webdesign-studenten.nl
 * @since      1.0.0
 *
 * @package    Woocommerce_dashboard_pro
 * @subpackage Woocommerce_dashboard_pro/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Woocommerce_dashboard_pro
 * @subpackage Woocommerce_dashboard_pro/includes
 * @author     Webdesign Studenten <info@webdesign-studenten.nl>
 */
class Woocommerce_dashboard_pro_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'woocommerce_dashboard_pro',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
